# unfi-rpi-cam

K8s code for deployment of a Raspberry Pi camera with bridge to Unifi Protect 
has motion support

Sets up a sinlge configmap that controls motion for movement detection and publishing to unifi-cam-proxy

Set up secrets for the control of the unifi-cam-proxy

Both are running as a single pod so that motion only needs to connet to 'localhost' to update the moption events

## Helm

add the registry

```
helm repo add unifi-cam-proxy https://gitlab.com/api/v4/projects/29985626/packages/helm/stable
```

deploy using helm
```
kubectl create ns unifi-cam-proxy
helm install test-cam unifi-cam-proxy/unifi-cam-proxy --set token=<YOUR_TOKEN>
```